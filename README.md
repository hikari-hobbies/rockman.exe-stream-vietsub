# Rockman.EXE Stream Vietsub
## Lệnh encode (chạy trên linux)
``` bash
ffmpeg -i <đường dẫn đến file raw> -vf "ass=<đường dẫn đến file sub>" -preset slow -c:v libx264 -crf 20 -c:a copy "<tên file xuất ra>"
```
## Tiến trình
|# | Tên tập | Trạng thái |
| ------ | ------ | ------ |
| 1 | Duo | Hoàn thành |
| 2 | Sự tận diệt của Trái Đất | Hoàn thành |
| 3 | Mối đe dọa từ Asteroid | Hoàn thành |
| 4 | Ngày kỉ niệm rau củ?! Đang tiến hành |


